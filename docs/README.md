# Документы и документация

Папка с документами и документацией.

| Файл                                                         | Комментарии                                                                               |
| ------------------------------------------------------------ | ----------------------------------------------------------------------------------------  |
|[suddela138.1.docx](suddela138.1.docx)                        | Обзор 138.1 статьи УК РФ и сбор 20 судебных дел за период 2017-2020 г.                    |                                   |
|[suddela274.docx](suddela274.docx)                            | Обзор 274 статьи УК РФ и сбор 20 судебных дел за период 2017-2020 г.                      |
